// const es para declarar la variable
const addCreditSubject = {
  addOption:
    ".MuiToolbar-root > a:nth-child(3) > button:nth-child(1) > span:nth-child(1)",
  selectDocumentType: {
    focusSelect: ".css-19bqh2r",
    selectDni: "#react-select-2-option-0",
    selectCe: "#react-select-2-option-1",
    selectRuc: "#react-select-2-option-2"

  },
  inputDocumentNumber: ".MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input",
  addButton: ".MuiButton-contained > .MuiButton-label"

};

describe("SAFE 2.0", () => {
  it("Ingreso a URL Safe 2.0", () => {
    cy.visit("/", { failOnStatusCode: false });
  });

  it('Hago clic en opción "Agregar"', () => {
    cy.get(addCreditSubject.addOption).click();
  });

  it('Selecciono Tipo de Documento"', () => {
    cy.get(addCreditSubject.selectDocumentType.focusSelect).click();
    cy.get(addCreditSubject.selectDocumentType.selectRuc).click();
  });

  it('Introduzco Número de Documento"', () => {
    cy.get(addCreditSubject.inputDocumentNumber).type("10164374209");
  });

  it('Hago clic en botón "Agregar"', () => {
    cy.get(addCreditSubject.addButton).click();
  });

  it('Valido valor del campo "DNI/RUC/Carnet de Extranjería', () => {
    cy.wait(5000);
    cy.get(
      "div.MuiGrid-grid-xs-12:nth-child(2) > div:nth-child(1) > p:nth-child(1)"
    ).should("contain", "10164374209");
  });

  it('Valido valor del campo "Nombre"', () => {
    cy.get(":nth-child(4) > .MuiPaper-root > .MuiTypography-root").should(
      "have.text",
      "JORGE LUIS BERNABE BURGA"
    );
  });

  it('Valido valor del campo "Tipo Persona"', () => {
    cy.get(":nth-child(6) > .MuiPaper-root > .MuiTypography-root").should(
      "have.text",
      "Persona Natural"
    );
  });

  it('Valido valor del campo "Fecha Inicio Actividad"', () => {
    cy.get(":nth-child(8) > .MuiPaper-root > .MuiTypography-root").should(
      "have.text",
      "18/05/1998"
    );
  });
  it('Valido valor del campo "Actividad Económica"', () => {
    cy.get(":nth-child(10) > .MuiPaper-root > .MuiTypography-root").should(
      "have.text",
      "RESTAURANTES, BARES Y CANTINAS"
    );
  });
  it('Valido texto del enlace Inf. Financiera"', () => {
    cy.get(
      "div.MuiGrid-direction-xs-column:nth-child(2) > div:nth-child(1) > p:nth-child(1)"
    ).should("have.text", "Para ver el detalle haga click enInf. Finaciera");
  });

  it('Hago clic en enlace Inf.Financiera"', () => {
    cy.get(".MuiTypography-body1 > .MuiTypography-root").click();
  });
});
